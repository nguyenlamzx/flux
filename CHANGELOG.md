# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2022-09-30

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux` - `v0.1.0+5`](#flux---v0105)
 - [`flux_lints` - `v0.0.2+3`](#flux_lints---v0023)
 - [`flux_localizations` - `v0.1.0+2`](#flux_localizations---v0102)
 - [`flux_navigation` - `v0.1.0+2`](#flux_navigation---v0102)
 - [`flux_theme` - `v0.0.1+3`](#flux_theme---v0013)
 - [`flux_theme_playground` - `v0.0.1+3`](#flux_theme_playground---v0013)
 - [`flux_widgets` - `v0.1.2+5`](#flux_widgets---v0125)

---

#### `flux` - `v0.1.0+5`

 - **FIX**: remove unused code.

#### `flux_lints` - `v0.0.2+3`

 - **FIX**: remove unused code.

#### `flux_localizations` - `v0.1.0+2`

 - **FIX**: remove unused code.

#### `flux_navigation` - `v0.1.0+2`

 - **FIX**: remove unused code.

#### `flux_theme` - `v0.0.1+3`

 - **FIX**: remove unused code.

#### `flux_theme_playground` - `v0.0.1+3`

 - **FIX**: remove unused code.

#### `flux_widgets` - `v0.1.2+5`

 - **FIX**: remove unused code.


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux_widgets` - `v0.1.2+4`](#flux_widgets---v0124)

---

#### `flux_widgets` - `v0.1.2+4`


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux_lints` - `v0.0.2+2`](#flux_lints---v0022)

---

#### `flux_lints` - `v0.0.2+2`

 - Bump "flux_lints" to `0.0.2+2`.


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux_widgets` - `v0.1.0+3`](#flux_widgets---v0103)

---

#### `flux_widgets` - `v0.1.0+3`


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux` - `v0.1.0+4`](#flux---v0104)

---

#### `flux` - `v0.1.0+4`

 - Bump "flux" to `0.1.0+4`.


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux` - `v0.1.0+3`](#flux---v0103)

---

#### `flux` - `v0.1.0+3`

 - Bump "flux" to `0.1.0+3`.


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux` - `v0.1.0+2`](#flux---v0102)
 - [`flux_theme` - `v0.0.1+2`](#flux_theme---v0012)
 - [`flux_theme_playground` - `v0.0.1+2`](#flux_theme_playground---v0012)
 - [`flux_widgets` - `v0.0.1+2`](#flux_widgets---v0012)

---

#### `flux` - `v0.1.0+2`

 - **FIX**: correct the version.

#### `flux_theme` - `v0.0.1+2`

 - **FIX**: correct the version.

#### `flux_theme_playground` - `v0.0.1+2`

 - **FIX**: correct the version.

#### `flux_widgets` - `v0.0.1+2`

 - **FIX**: correct the version.


## 2022-09-19

### Changes

---

Packages with breaking changes:

 - There are no breaking changes in this release.

Packages with other changes:

 - [`flux_lints` - `v0.0.1+2`](#flux_lints---v0012)

---

#### `flux_lints` - `v0.0.1+2`

 - **FIX**: correct the version.

