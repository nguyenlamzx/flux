## 0.1.0+2

 - **FIX**: remove unused code.

---
type: changelog
---

# Change Log

## 0.1.0+1

- feat: initial commit 🎉
