## 0.1.0+5

 - **FIX**: remove unused code.

---
type: changelog
---

# Change Log

## 0.1.0+4

 - Bump "flux" to `0.1.0+4`.

## 0.1.0+3

 - Bump "flux" to `0.1.0+3`.

## 0.1.0+2

 - **FIX**: correct the version.

