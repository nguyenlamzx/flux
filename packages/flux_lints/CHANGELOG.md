## 0.0.2+3

 - **FIX**: remove unused code.

---
type: changelog
---

# Change Log

## 0.0.2+2

 - Bump "flux_lints" to `0.0.2+2`.

## 0.0.1+2

 - **FIX**: correct the version.

