// ignore_for_file: prefer_const_constructors

import 'package:flutter_test/flutter_test.dart';
import 'package:flux_localizations/flux_localizations.dart';

void main() {
  group('FluxLocalizations', () {
    test('can be instantiated', () {
      expect(FluxLocalizations(), isNotNull);
    });
  });
}
